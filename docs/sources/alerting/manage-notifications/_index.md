---
canonical: https://grafana.com/docs/grafana/latest/alerting/manage-notifications/
description: Manage alert notifications
keywords:
  - grafana
  - alert
  - notifications
labels:
  products:
    - cloud
    - enterprise
    - oss
menuTitle: Manage
title: Manage your alerts
weight: 160
---

# Manage your alerts

Once you have set up your alert rules, contact points, and notification policies, you can use Grafana Alerting to:

[Create silences]({{< relref "./create-silence" >}})

[Create mute timings]({{< relref "./mute-timings" >}})

[Declare incidents from firing alerts]({{< relref "./declare-incident-from-alert" >}})

[View the state and health of alert rules]({{< relref "./view-state-health" >}})

[View and filter alert rules]({{< relref "./view-alert-rules" >}})
